﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateProject
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public int ExprirationHours { get; set; }
        public string Issuer { get; set; }
        public IEnumerable<string> ValidIn { get; set; }
        public OAuthApp OAuth { get; set; }
    }

    public class OAuthApp
    {
        public OAuthData Facebook { get; set; }
        public OAuthData Twitter { get; set; }
        public OAuthData Google { get; set; }
        public OAuthData Microsoft { get; set; }
    }

    public class OAuthData
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
    }
}
