﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CorporateProject.Models;
using Microsoft.AspNetCore.Authorization;
using CorporateProject.Data;

namespace CorporateProject.Controllers
{
    [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class TypeEnterprisesController : ControllerBase
    {
        private readonly CorporateProjectContext _context;

        public TypeEnterprisesController(CorporateProjectContext context)
        {
            _context = context;
        }

        // GET: api/TypeEnterprises
        [HttpGet]
        public IEnumerable<TypeEnterprise> GetTypeEnterprise()
        {
            return _context.TypeEnterprise;
        }

        // GET: api/TypeEnterprises/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTypeEnterprise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var typeEnterprise = await _context.TypeEnterprise.FindAsync(id);

            if (typeEnterprise == null)
            {
                return NotFound();
            }

            return Ok(typeEnterprise);
        }

        // PUT: api/TypeEnterprises/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTypeEnterprise([FromRoute] int id, [FromBody] TypeEnterprise typeEnterprise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != typeEnterprise.Id)
            {
                return BadRequest();
            }

            _context.Entry(typeEnterprise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeEnterpriseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TypeEnterprises
        [HttpPost]
        public async Task<IActionResult> PostTypeEnterprise([FromBody] TypeEnterprise typeEnterprise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TypeEnterprise.Add(typeEnterprise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTypeEnterprise", new { id = typeEnterprise.Id }, typeEnterprise);
        }

        // DELETE: api/TypeEnterprises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTypeEnterprise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var typeEnterprise = await _context.TypeEnterprise.FindAsync(id);
            if (typeEnterprise == null)
            {
                return NotFound();
            }

            _context.TypeEnterprise.Remove(typeEnterprise);
            await _context.SaveChangesAsync();

            return Ok(typeEnterprise);
        }

        private bool TypeEnterpriseExists(int id)
        {
            return _context.TypeEnterprise.Any(e => e.Id == id);
        }
    }
}