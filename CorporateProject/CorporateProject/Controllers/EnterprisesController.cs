﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using CorporateProject.Data;
using CorporateProject.Models;

namespace CorporateProject.Controllers
{
    [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly CorporateProjectContext _context;

        public EnterprisesController(CorporateProjectContext context)
        {
            _context = context;
        }

        // GET: api/Enterprises
        [HttpGet]
        public IEnumerable<Enterprise> GetEnterprise(int? enterprise_types, string name)
        {
            return _context.Enterprise
                .Include(e => e.Type)
                .Where(e => (!enterprise_types.HasValue || e.TypeId == enterprise_types.Value) &&
                            (string.IsNullOrEmpty(name) || e.Name.Contains(name)));
        }

        // GET: api/Enterprises/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEnterprise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //CorporateContext _context = new CorporateContext();
            var enterprise = await _context.Enterprise.FindAsync(id);//.Include(e => e.Type).FirstAsync(e => e.Id == id);

            if (enterprise == null)
            {
                return NotFound();
            }

            return Ok(enterprise);
        }

        // PUT: api/Enterprises/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEnterprise([FromRoute] int id, [FromBody] Enterprise enterprise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != enterprise.Id)
            {
                return BadRequest();
            }

            //CorporateContext _context = new CorporateContext();
            _context.Entry(enterprise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnterpriseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Enterprises
        [HttpPost]
        public async Task<IActionResult> PostEnterprise([FromBody] Enterprise enterprise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //CorporateContext _context = new CorporateContext();
            _context.Enterprise.Add(enterprise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEnterprise", new { id = enterprise.Id }, enterprise);
        }

        // DELETE: api/Enterprises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEnterprise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //CorporateContext _context = new CorporateContext();
            var enterprise = await _context.Enterprise.FindAsync(id);
            if (enterprise == null)
            {
                return NotFound();
            }

            _context.Enterprise.Remove(enterprise);
            await _context.SaveChangesAsync();

            return Ok(enterprise);
        }

        private bool EnterpriseExists(int id)
        {
            //CorporateContext _context = new CorporateContext();
            return _context.Enterprise.Any(e => e.Id == id);
        }
    }
}