﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using CorporateProject.Models;

namespace CorporateProject.Data
{
    public class CorporateProjectContext : IdentityDbContext
    {
        public CorporateProjectContext (DbContextOptions<CorporateProjectContext> options)
            : base(options)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
         
        public DbSet<Enterprise> Enterprise { get; set; }

        public DbSet<TypeEnterprise> TypeEnterprise { get; set; }
    }
}
