﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateProject.Models
{
    public class SessionToken
    {
        [JsonProperty("access-token")]
        public string AccessToken { get; set; }

        [JsonProperty("client")]
        public string Client { get; set; }

        [JsonProperty("uid")]
        public string UID { get; set; }
    }
}
