﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CorporateProject.Models
{
    public class TypeEnterprise
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
