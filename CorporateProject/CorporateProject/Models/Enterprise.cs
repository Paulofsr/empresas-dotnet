﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CorporateProject.Models
{
    public class Enterprise
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }

        [ForeignKey("TypeId")]
        public virtual TypeEnterprise Type { get; set; }
    }
}
